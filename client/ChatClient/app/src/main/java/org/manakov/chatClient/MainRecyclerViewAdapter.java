package org.manakov.chatClient;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MainRecyclerViewAdapter extends RecyclerView.Adapter<MainRecyclerViewAdapter.ViewHolder> {

    private ArrayList<String> list;

    public MainRecyclerViewAdapter(ArrayList<String> list){
        this.list = list;
    }

    @Override
    public int getItemCount(){
        return this.list.size();
    }

    @Override
    @NonNull
    public MainRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_layout,
                parent,
                false
        );
        return new ViewHolder(view);
    }

    @Override
    @NonNull
    public void onBindViewHolder(@NonNull MainRecyclerViewAdapter.ViewHolder viewHolder, int position){
        viewHolder.textView.setText(
                list.get(position)
        );
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView textView;

        private ViewHolder(View view){
            super(view);

            textView = view.findViewById(R.id.stringTextView);
        }
    }
}
