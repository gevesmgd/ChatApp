package org.manakov.chatClient;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mainRecyclerView;
    private MainRecyclerViewAdapter mainRecyclerViewAdapter;
    private ArrayList<String> list;

    private Button button;
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initRecyclerView();


        fillList();
        setUpButton();
    }

    public void fillList(){
        this.list.add("first");
        this.list.add("second");
        this.list.add("third");
        this.list.add("fourth");
        this.list.add("fifth");

        this.mainRecyclerViewAdapter.notifyDataSetChanged();
    }

    public void setUpButton(){
        this.button = findViewById(R.id.submitButton);
        this.editText = findViewById(R.id.mainEditText);

        this.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list.add(editText.getText().toString());
                mainRecyclerViewAdapter.notifyDataSetChanged();
            }
        });
    }

    public void initRecyclerView(){
        this.mainRecyclerView = findViewById(R.id.mainRecyclerView);
        this.mainRecyclerView.setVisibility(View.VISIBLE);
        this.mainRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        this.list = new ArrayList<>();

        this.mainRecyclerViewAdapter = new MainRecyclerViewAdapter(list);

        this.mainRecyclerView.setAdapter(mainRecyclerViewAdapter);

        this.mainRecyclerViewAdapter.notifyDataSetChanged();
    }

}